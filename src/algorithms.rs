pub mod sorts {
    pub fn bubble_sort(mut array: Vec<u64>) -> Vec<u64> {
        let mut change: bool = false;
        loop {
            for j in 0..array.len() - 1 {
                if array[j] > array[j + 1] {
                    array.swap(j, j + 1);
                    change = true;
                }
            }
            if change {
                break;
            }
        }
        array
    }
    pub fn insetion_sort(mut array: Vec<u64>) -> Vec<u64> {
        for i in 1..array.len() {
            let mut j = i;
            while j > 0 && array[j - 1] > array[j] {
                array.swap(j, j - 1);
                j -= 1;
            }
        }
        array
    }
    /*
     * What the fuck did I wrote here?
     * This is a lot slower than bubble sort...
     * I don't know why.
     */
    pub fn quick_sort(mut array: Vec<u64>) -> Vec<u64> {
        if array.len() <= 1 {
            return array;
        }
        let pivot = array.remove(0);
        let mut left: Vec<u64> = Vec::new();
        let mut right: Vec<u64> = Vec::new();
        for i in 0..array.len() {
            if array[i] < pivot {
                left.push(array[i]);
            } else {
                right.push(array[i]);
            }
        }
        let mut result: Vec<u64> = Vec::new();
        result.append(&mut quick_sort(left));
        result.push(pivot);
        result.append(&mut quick_sort(right));
        result
    }
    pub fn stalin_sort(array: Vec<u64>) -> Vec<u64> {
        let mut result: Vec<u64> = Vec::new();
        let mut bigger = 0;

        for i in 0..result.len() {
            if array[i] >= bigger {
                bigger = array[i];
                result.push(bigger);
            }
        }
        result
    }
}
