pub mod handler {
    pub fn number_of_integers(prompt: &str) -> u64 {
        println!("{}", prompt);
        let mut input = String::new();
        std::io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");
        let input = input.trim().parse().expect("Please type a number!");
        input
    }
    pub fn sort_type(prompt: &str) -> u8 {
        println!("{}", prompt);
        let mut input = String::new();
        std::io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");
        let input = input.trim().parse::<u8>().expect("Please type a number!");
        input
    }
}
