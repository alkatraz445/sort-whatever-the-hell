pub mod generate {
    pub fn random_vector(input: u64) -> Vec<u64> {
        let mut rand_vector: Vec<u64> = Vec::new();
        for _ in 0..input {
            rand_vector.push(rand::random());
        }
        rand_vector
    }
}
