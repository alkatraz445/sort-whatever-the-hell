pub fn write_to_file(data: String) {
    let mut f = std::fs::OpenOptions::new()
        .append(true)
        .create(true)
        .open("results.txt")
        .unwrap();
    std::io::Write::write_all(&mut f, data.as_bytes()).expect("Unable to write to file");
}
