mod algorithms;
mod file;
mod generator;
mod input;
fn main() {
    println!("Crab sorting 🦀\n1. Bubble sort\n2. Insertion sort\n3. Quick sort\n4. Stalin sort 🛠\n9. Quit");

    let sort_type = input::handler::sort_type("Please choose a sorting algorithm: ");

    let picked_sort = match sort_type {
        1 => algorithms::sorts::bubble_sort,
        2 => algorithms::sorts::insetion_sort,
        3 => algorithms::sorts::quick_sort,
        4 => algorithms::sorts::stalin_sort,
        9 => panic!("Quit"),
        _ => panic!("There is no sorting algorithm assigned to this number"),
    };
    let picked_sort_as_string = match sort_type {
        1 => "Bubble sort",
        2 => "Insertion sort",
        3 => "Quick sort",
        4 => "Stalin sort",
        _ => "",
    };
    let n_input = input::handler::number_of_integers("Enter array size: ");
    let start_of_generation = std::time::Instant::now();
    let array: Vec<u64> = generator::generate::random_vector(n_input);
    let end_of_generation = start_of_generation.elapsed();

    let start_of_sort = std::time::Instant::now();
    picked_sort(array);
    let elapsed_time_sort = start_of_sort.elapsed();
    let data = format!(
        "Algorithm: {} | n = {} | Processing time: {} ms\n",
        picked_sort_as_string,
        n_input,
        elapsed_time_sort.as_millis() + end_of_generation.as_millis()
    );
    println!(
        "The sorting took: {} ms\nThe number generation took: {} ms",
        elapsed_time_sort.as_millis(),
        end_of_generation.as_millis()
    );
    file::write_to_file(data);
}
